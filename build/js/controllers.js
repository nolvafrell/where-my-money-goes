var app = angular.module('app', ['ui.bootstrap']);


// Api data Controller
app.controller('ApiData', function ApiData($scope, $http, $modal) {

  var queue = 'http://test-api.kuria.tshdev.io/',
    queue_nav;

  // Stan początkowy
  $http({
    url: queue,
    method: 'GET',
  }).success(function(data) {
    $scope.details = data;
  }).error(function(data) {
    $scope.error = "An error occured!";
  });

  // Wyszukiwanie
  $scope.search = function() {
    $http({
      url: queue,
      method: 'GET',
      params: {
        query: $scope.query,
        rating: $scope.rating
      }
    }).success(function(data) {
      $scope.details = data;
    }).error(function(data) {
      $scope.error = "An error occured!";
    });
  };

  // Reset wyszukiwania
  $scope.reset = function() {
    $scope.query = undefined;
    $scope.rating = undefined;
    $http({
      url: queue,
      method: 'GET',
    }).success(function(data) {
      $scope.details = data;
    }).error(function(data) {
      $scope.error = "An error occured!";
    });
  };

  // Nothing found
  $scope.nothing = "Nothing found...";

  // Rating
  $scope.getBluePoints = function(points) {
    return new Array(parseInt(points));
  };

  $scope.getGreyPoints = function(points) {
    return new Array(5 - parseInt(points));
  };

  // Paginacja
  $scope.nav = function(key) {
    item = $scope.details.pagination.links[key];
    queue_nav = queue + '?' + item;
    $http.get(queue_nav)
      .success(function(data) {
        $scope.details = data;
        $scope.url = queue_nav;
      });
  };
  $scope.nav_prev = function(key) {
    item = parseInt(key);
    return $scope.nav(item - 1);
  };
  $scope.nav_next = function(key) {
    item = parseInt(key);
    return $scope.nav(item + 1);
  };
  $scope.button = function(key) {
    item = parseInt(key) + 1;
    return item;
  };

  // MODAL WINDOW
  $scope.open = function(_payment) {

    var modalInstance = $modal.open({
      controller: "ModalController",
      templateUrl: 'partials/modal.html',
      resolve: {
        payment: function() {
          return _payment;
        }
      }
    });
  };

});


// Modal window Controller
app.controller('ModalController', function($scope, $modalInstance, payment) {
  $scope.payment = payment;

  // Rating
  $scope.getBluePoints = function(points) {
    return new Array(parseInt(points));
  };

  $scope.getGreyPoints = function(points) {
    return new Array(5 - parseInt(points));
  };

  // 'Back' button
  $scope.back = function() {
    $modalInstance.close();
  };
});
