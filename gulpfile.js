// ---------------------------------------------
// Konfiguracja projektu
// ---------------------------------------------

var   project       = 'tsh',
      url           = 'build/'
      source        = 'dev/',
      dest          = 'build/',

      syncOpts = {
        ports: {
          min: 3000,
          max: 3001
        },
        server: {
            baseDir: url,
            index: "index.html"
        },
        startPath: null,
        ghostMode: {
            clicks: true,
            links: true,
            forms: true,
            scroll: true
        },
        open: false,
        notify: true
      };


// ---------------------------------------------
// Moduły GULPa
// ---------------------------------------------

var   gulp          = require('gulp'),
      sass          = require('gulp-sass'),
      browsersync   = require('browser-sync'),
      reload        = browsersync.reload,
      pleeease      = require('gulp-pleeease'),
      autoprefixer  = require('gulp-autoprefixer'),
      rimraf        = require('gulp-rimraf'),
      runSequence   = require('run-sequence');


// ---------------------------------------------
// Zarzadzanie plikami
// ---------------------------------------------

clean = [ dest + '**/*' ];

scss = {
        in: source + 'scss/style.scss',
        watch: source + 'scss/**/*',
        out: dest + 'css',

        sassOpts: {
          outputStyle: 'expanded',
          imagePath: '../images',
          precision: 3,
          errLogToConsole: true
        },

        pleeeaseOpts: {
          autoprefixer: { browsers: ['last 4 versions', '> 2%', 'ios 6'] },
          filters : { "oldIE": true },
          rem: ['16px'],
          pseudoElements: true,
          opacity: true,
          mqpacker: true,
          minifier: false
        }
},

files = {
  in: [
    source + '**/*.html',
    source + '**/*.js',
    source + '**/*.css',
    source + '**/*.jpg',
    source + '**/*.png',
    source + '**/*.svg',
    source + '**/*.gif'
  ],
  watch: [
    source + '**/*.html',
    source + '**/*.js',
    source + '**/*.css',
    source + '**/*.jpg',
    source + '**/*.png',
    source + '**/*.svg',
    source + '**/*.gif'
  ],
  out: dest
};



// ---------------------------------------------
// GULP!
// ---------------------------------------------

// RELOAD

gulp.task('browsersync', function() {
  browsersync(syncOpts);
})


// SASS

gulp.task('sass', function() {
  return  gulp.src(scss.in)
          .pipe(sass(scss.sassOpts).on('error', sass.logError))
          .pipe(pleeease(scss.pleeeaseOpts))
          .pipe(gulp.dest(scss.out))
          .pipe(browsersync.reload({
            stream: true
          }));
});


// FILES

gulp.task('files', function() {
  return gulp.src(files.in)
  .pipe(gulp.dest(files.out));
});


// CLEAN BUILD

gulp.task('clean', function() {
  gulp.src(clean, {read: false})
  .pipe(rimraf());
});


// WATCH

gulp.task('watch',  ['files', 'sass', 'browsersync'], function(){

  // FILES changes
	gulp.watch(files.watch, ['files', reload]);

	// SASS changes
	gulp.watch([scss.watch], ['sass']);

});


// ---------------------------------------------
// DEFAULT
// ---------------------------------------------

gulp.task('default', function(cb){
  runSequence('clean', ['watch', 'browsersync'], cb )
});
